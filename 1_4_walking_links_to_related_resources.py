from orbtalk_api_utilities import setup_coreapi

# settings start
schema_url = "https://rest.orbtalk.com/v1/swagger.json"
auth_endpoint = "https://auth.orbtalk.com:8888/v1/oauth2/tokeninfo"
auth_username = "ORB1234567"
auth_password = "xxxxxx"
domain = "ORB1234567"
extension = "1000"
# settings end

client, schema = setup_coreapi(auth_endpoint, auth_username, auth_password, schema_url)

print "Reading extension {0}...".format(extension)
doc_extension = client.action(schema, ['pbx.extension', 'resources.pbx.extension.read'], params={
    "domainId": domain,
    "id": extension
})
print "...read extension result:"
print doc_extension

print "Reading related domain resource from extension document ..."
doc_domain = client.action(doc_extension, ["relations", "domain"])
print "...read domain result:"
print doc_domain
