from orbtalk_api_utilities import setup_coreapi

# settings start
schema_url = "https://rest.orbtalk.com/v1/swagger.json"
auth_endpoint = "https://auth.orbtalk.com:8888/v1/oauth2/tokeninfo"
auth_username = "ORB1234567"
auth_password = "xxxxxx"
domain = "ORB1234567"
queue_id = "3001"
queue_name = "Q1"
device = "1000"
# settings end

client, schema = setup_coreapi(auth_endpoint, auth_username, auth_password, schema_url)

print "Creating new Queue {0} called {1}".format(queue_id, queue_name)
doc_queue = client.action(schema, ['pbx.queue', 'resources.pbx.queue.create'], params={
    "queue": {
        "domainId": domain,
        "id": queue_id,
        "name": queue_name,
        "type": "roundRobin",
        "maxTime": 10,
        "waitLimit": 11,
        "maxQueueLength": 3,
        "agentRequired": True,
        "inboundNumbers": [
            "441273123456"
        ]
    }
})
print "Create queue result:"
print doc_queue

print "Create an Agent by adding device {0} into {1}({2})".format(device, queue_id, queue_name)
doc_queue = client.action(schema, ['pbx.agent', 'resources.pbx.agent.create'], params={
    "agent": {
        "domainId": domain,
        "queueId": queue_id,
        "id": device
    }
})
print "Create queue result:"
print doc_queue
