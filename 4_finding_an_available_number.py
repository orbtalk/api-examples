from orbtalk_api_utilities import setup_coreapi

# settings start
schema_url = "https://rest.orbtalk.com/v1/swagger.json"
auth_endpoint = "https://auth.orbtalk.com:8888/v1/oauth2/tokeninfo"
auth_username = "ORB1234567"
auth_password = "xxxxxx"
domain = "ORB1234567"
# settings end

client, schema = setup_coreapi(auth_endpoint, auth_username, auth_password, schema_url)

print "Listing numbers on domain."
doc_numbers = client.action(schema, ['pbx.number', 'resources.pbx.number.list'], params={
    "domainId": domain
})
print "List numbers result:"
print doc_numbers
