import json
import requests


def get_token(auth_host, auth_username, auth_password):
    # "payload" is our x-www-form-urlencoded request body.
    # We just need to insert our username and password into this string.
    payload = "grant_type=password&username={0}&password={1}".format(auth_username, auth_password)
    # now we make our POST request to the API.
    response = requests.request("POST", auth_host, data=payload, headers={
        'content-type': "application/x-www-form-urlencoded"
    })
    response.raise_for_status()
    # the text in the response body is JSON format, so convert it to a dictionary
    response_dict = json.loads(response.text)
    return response_dict["access_token"]

# settings start
username = "ORB1234567"
password = "xxxxxxxxxx"
# settings end

try:
    access_token = get_token("https://auth.orbtalk.com:8888/v1/oauth2/tokeninfo", username, password)
    print "We obtained access token: {0}".format(access_token)
except Exception as e:
    # if we had some kind of error, print it out here.
    print "There was an error. Message:"
    print e
