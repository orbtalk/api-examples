from orbtalk_api_utilities import setup_coreapi

# settings start
schema_url = "https://rest.orbtalk.com/v1/swagger.json"
auth_endpoint = "https://auth.orbtalk.com:8888/v1/oauth2/tokeninfo"
auth_username = "ORB1234567"
auth_password = "xxxxxx"
domain = "ORB1234567"
report_name = "outbound-report-24hrs"
# settings end

client, schema = setup_coreapi(auth_endpoint, auth_username, auth_password, schema_url)

doc_report_data = client.action(schema, ['report.cdr', 'resources.report.cdr.read_by_name'], params={
    "domainId": domain,
    "name": report_name,
    "runtime": "2017-02-15T11:00:00.656485+0000"
})

print "Report read result:"
print doc_report_data
