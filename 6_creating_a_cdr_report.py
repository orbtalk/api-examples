from orbtalk_api_utilities import setup_coreapi

# settings start
schema_url = "https://rest.orbtalk.com/v1/swagger.json"
auth_endpoint = "https://auth.orbtalk.com:8888/v1/oauth2/tokeninfo"
auth_username = "ORB1234567"
auth_password = "xxxxxx"
domain = "ORB1234567"
report_name = "outbound-report-24hrs"
# settings end

client, schema = setup_coreapi(auth_endpoint, auth_username, auth_password, schema_url)

report_meta_description = {
  "domainId": domain,
  "dataSources": ["cdr"],
  "filters": {
    "eq": {
      "cdr.direction": "out"
    }
  },
  "frequency": "realtime",
  "fields": [
    "cdr.direction",
    "cdr.timeStart",
    "cdr.fromName",
    "cdr.fromExtension",
    "cdr.toNumber",
    "cdr.secondsTalking"
  ],
  "dataFrom": "1|day",
  "dataTo": "now",
  "name": report_name,
  "summaries": {
      "cdr.fromExtension": ["cdr.secondsTalking", "cdr.toNumber"]
  },
  "type": "cdr"
}

doc_report = client.action(schema, ['report.meta', 'resources.report.meta.create'], params={
    "meta": report_meta_description
})

print "Report create result:"
print doc_report
