from orbtalk_api_utilities import setup_coreapi

# settings start
schema_url = "https://rest.orbtalk.com/v1/swagger.json"
auth_endpoint = "https://auth.orbtalk.com:8888/v1/oauth2/tokeninfo"
auth_username = "ORB1234567"
auth_password = "xxxxxx"
domain = "ORB1234567"
extension = "1000"
# settings end

client, schema = setup_coreapi(auth_endpoint, auth_username, auth_password, schema_url)

print "Creating new extension {0}.".format(extension)
doc_extension = client.action(schema, ['pbx.extension', 'resources.pbx.extension.create'], params={
    "extension": {
        "domainId": domain,
        "id": extension,
        "firstName": "Manager",
        "lastName": "User",
        "role": "manager",
        "email": "manager.user@some-domain.com",
        "loginName": "{0}@{1}.cloud.orbtalk.com".format(extension, domain),
        "callidName": "The Manager"
    }
})
print "Create extension result:"
print doc_extension

print "Creating new primary device {0} for extension {0}".format(extension)
doc_device1 = client.action(schema, ['pbx.device', 'resources.pbx.device.create'], params={
    "device": {
      "id": extension,
      "domainId": domain,
      "extensionId": extension
    }
})
print "Create device result:"
print doc_device1

print "Creating new device for {0}h for extension {0}".format(extension)
doc_device2 = client.action(schema, ['pbx.device', 'resources.pbx.device.create'], params={
    "device": {
      "id": "{0}h".format(extension),
      "domainId": domain,
      "extensionId": extension
    }
})
print "Create device result:"
print doc_device2
