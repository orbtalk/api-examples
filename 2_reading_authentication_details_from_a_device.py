from orbtalk_api_utilities import setup_coreapi

# settings start
schema_url = "https://rest.orbtalk.com/v1/swagger.json"
auth_endpoint = "https://auth.orbtalk.com:8888/v1/oauth2/tokeninfo"
auth_username = "ORB1234567"
auth_password = "xxxxxx"
domain = "ORB1234567"
device = "1000h"
# settings end

client, schema = setup_coreapi(auth_endpoint, auth_username, auth_password, schema_url)

print "Reading details of device {0}.".format(device)
doc_device = client.action(schema, ['pbx.device', 'resources.pbx.device.read'], params={
        "domainId": domain,
        "id": device
})
print "Read device result:"
print doc_device
