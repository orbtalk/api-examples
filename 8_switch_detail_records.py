from orbtalk_api_utilities import setup_coreapi

# settings start
schema_url = "https://rest.orbtalk.com/v1/swagger.json"
auth_endpoint = "https://auth.orbtalk.com:8888/v1/oauth2/tokeninfo"
auth_username = "ORB1234567"
auth_password = "xxxxxx"
# in this module we are not only dealing with Cloud PBX customers who own a PBX domain,
# so instead we are using the customer ID instead of domain. It's still your ORBXXXXXX account number though!
customer_id = "ORB1234567"
date_from = "2017-04-01"
date_to = "2017-04-30"
date_modified = "2017-05-01"
# settings end

client, schema = setup_coreapi(auth_endpoint, auth_username, auth_password, schema_url)

print "Creating a new SDR report from {0} to {1}".format(date_from, date_to)
doc_create_sdr = client.action(schema, ['account.sdr', 'resources.account.sdr.create'], params={
    "sdr": {
        "customerId": customer_id,
        "from": date_from,
        "to": date_to
    }
})
print "Create SDR report result:"
print doc_create_sdr

print "Creating a new SDR report from {0} to {1}".format(date_from, date_to)
doc_list_sdrs = client.action(schema, ['account.sdr', 'resources.account.sdr.list'], params={
    "customerId": customer_id
})
print "Listing SDR reports result:"
print doc_list_sdrs

print "Picking up SDR changes from {0} to {1} and modified since {2}".format(date_from, date_to, date_modified)
doc_sdr_changes = client.action(schema, ['account.sdr', 'resources.account.sdr.create'], params={
    "sdr": {
        "customerId": customer_id,
        "from": date_from,
        "to": date_to,
        "modifiedSince": date_modified
    }
})
print "SDR changes report result:"
print doc_sdr_changes
