from orbtalk_api_utilities import setup_coreapi

# settings start
schema_url = "https://rest.orbtalk.com/v1/swagger.json"
auth_endpoint = "https://auth.orbtalk.com:8888/v1/oauth2/tokeninfo"
auth_username = "ORB1234567"
auth_password = "xxxxxx"
domain = "ORB1234567"
extension = "1000"
destination = "01273123456"
# settings end

client, schema = setup_coreapi(auth_endpoint, auth_username, auth_password, schema_url)

print "Creating new call for extension 1000..."
doc_call = client.action(schema, ['pbx.call', 'resources.pbx.call.create'], params={
    "call": {
        "domainId": domain,
        "origExtension": extension,
        "destination": destination
    }
})
print "...create call result was:"
print doc_call
